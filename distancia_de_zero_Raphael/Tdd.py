import unittest
from conta_zero import conta_zeros

class MyTestCase(unittest.TestCase):
    def test_something(self):
        self.assertEqual(conta_zeros("210002200"), 3)
    def test_something(self):
        self.assertEqual(conta_zeros("abc"), 0)
    def test_something(self):
        self.assertEqual(conta_zeros(""), 0)
    def test_something(self):
        self.assertEqual(conta_zeros("00012340001234000"), 3)
    def test_something(self):
        self.assertEqual(conta_zeros("000123400123400"), 3)
    def test_something(self):
        self.assertEqual(conta_zeros("001234000123400"), 3)
    def test_something(self):
        self.assertEqual(conta_zeros("001234001234000"), 3)
    def test_something(self):
        self.assertEqual(conta_zeros("0"),1)

if __name__ == '__main__':
    unittest.main()

